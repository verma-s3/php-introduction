<?php

// set error display and reporting level
// could be set manually inside php.ini
// which would make them global

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// set Database Credentials

define('DB_DSN', 'mysql:host=localhost;dbname=intro4');
define('DB_USER', 'root');
define('DB_PASS', '');


$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

?>