<?php

// including config file and function files... 
require 'config.php';
require 'functions.php';

if('GET'==$_SERVER['REQUEST_METHOD']){
	$query = "SELECT * FROM cars";

	$stmt = $dbh->prepare($query);
    
    $params = array(':id'=>$_GET['id']);
	$stmt->execute($params);

	$result = $stmt->fetch(PDO::FETCH_ASSOC);

}
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
</head>
<body>
	<h1><?=$result['make']?> <?=$result['model']?></h1>

	<ul>
	    
    	<li>ID: <?=$result['id']?></li>
        <li>MAKE: <?=$result['make']?></li>
        <li>MODEL: <?=$result['model']?></li>
        <li>YEAR: <?=$result['year']?></li>
        <li>COLOR: <?=$result['color']?></li>
        <li>COST: <?=$result['cost']?></li>
        <li>PRICE: <?=$result['price']?></li>
        <li>CREATED_ID: <?=$result['created_at']?></li>
	    
	    
	</ul>

	<p>
		<a href="add_data.php" title="adding data">ADD NEW CAR</a>
    </p>
</body>
</html>