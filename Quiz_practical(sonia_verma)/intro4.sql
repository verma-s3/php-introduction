# Creating Intro sql database
# 

CREATE DATABASE intro4;


-- using database intro4
USE intro4;


-- create table for cars

CREATE TABLE cars(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
make VARCHAR(255),
model VARCHAR(255),
year INT(4),
color VARCHAR(255),
cost FLOAT,
price FLOAT,
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
)engine=InnoDB;

