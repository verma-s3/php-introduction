<?php
$title = "ADD NEW CARS";
// including config file and function files... 
require 'config.php';
require 'functions.php';

// empty errors array created
$errors = [];

// test for post method
if('POST' == $_SERVER['REQUEST_METHOD']) {

    //make validation
	if(empty($_POST['make'])){
	  	$errors['make'] = "Make field is required. Please Enter a value";
	}
	elseif(strlen($_POST['make']) < 3){
	  	$errors['make'] = "Please provide atleast 3 characters" ;
	}

	//model validation
	if(empty($_POST['model'])){
	  	$errors['model'] = "Model field is required. Please Enter a value";
	}
	elseif(strlen($_POST['model']) < 3){
	  	$errors['model'] = "Please provide atleast 3 characters" ;
	}
	  
	//Year validation
	if(empty($_POST['year'])){
	  	$errors['year'] = "Year is required. Please enter a value";
	}
	elseif(!filter_var($_POST['year'],FILTER_VALIDATE_INT)){
		$errors['year'] = "Please enter a number Only";
	}

	//color validation
	if(empty($_POST['color'])){
	  	$errors['color'] = "color field is required. Please Enter a value";
	}
	elseif(strlen($_POST['color']) < 3){
	  	$errors['color'] = "Please provide atleast 3 characters" ;
	}
    
	//cost validation
	if(empty($_POST['cost'])){
	  	$errors['cost'] = "Cost is required. Please enter a value";
	}
	elseif(!filter_var($_POST['cost'],FILTER_VALIDATE_FLOAT)){
		$errors['cost'] = "Please enter a number Only";
	}

	//Year validation
	if(empty($_POST['price'])){
	  	$errors['price'] = "Price is required. Please enter a value";
	}
	elseif(!filter_var($_POST['price'],FILTER_VALIDATE_FLOAT)){
		$errors['price'] = "Please enter a number Only";
	}

    if(empty($errors)) {

        // create query
        $query = 'INSERT INTO
                    cars
                    (make, model, year, color, cost, price)
                    VALUES
                    (:make, :model, :year, :color, :cost, :price)';

        // prepare the query
        $stmt = $dbh->prepare($query);
        
        // Set the parameter values
        $params = array(
            ':make' => $_POST['make'],
            ':model' => $_POST['model'],
            ':year' => $_POST['year'],
            ':color' => $_POST['color'],
            ':cost' => $_POST['cost'],
            ':price' => $_POST['price']
        );

        // execute query with the parameters
        $stmt->execute($params);

        // get the ID of the record we just created
        $id = $dbh->lastInsertId();

        // if insert is okay redirect to page to show the  new record
        if($id) {
            header('Location:show_data.php?id='.$id);
            exit;
        } else {
            // if insert is NOT okay set a brief error message to
            // display on this page
            $errors[] = 'There was a problem inserting the record!';
        }
     } // end errors empty

} // end test for post



?><!DOCTYPE html>
<html>
	<head>
		<title><?=$title?></title>
	</head>
	<style>
	    .error {
            color: #ff0000;
            font-weight: 500;
            margin-left: 10px;
        }
	</style>
<body>
	<h1><?=$title?></h1>
  <form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
    <p>
    	<label for="make">Make</label>
        <input type="text" name="make" id="make" value="<?php
                if(!empty($_POST['make'])) {
                    echo esc_attr($_POST['make']);
                }
            ?>" />
        <?php if(!empty($errors['make'])) : ?>
            <span class="error"><?=$errors['make']?></span>
        <?php endif; ?>
    </p>

    <p>
    	<label for="model">Model</label>
        <input type="text" name="model" id="model" value="<?php
                if(!empty($_POST['model'])) {
                    echo esc_attr($_POST['model']);
                }
            ?>" />
        <?php if(!empty($errors['model'])) : ?>
            <span class="error"><?=$errors['model']?></span>
        <?php endif; ?>
    </p>

    <p>
    	<label for="year">Year</label>
        <input type="text" name="year" id="year" value="<?php
                if(!empty($_POST['year'])) {
                    echo esc_attr($_POST['year']);
                }
            ?>" />
        <?php if(!empty($errors['year'])) : ?>
            <span class="error"><?=$errors['year']?></span>
        <?php endif; ?>
    </p>

    <p>
    	<label for="color">Color</label>
        <input type="text" name="color" id="color" value="<?php
                if(!empty($_POST['color'])) {
                    echo esc_attr($_POST['color']);
                }
            ?>" />
        <?php if(!empty($errors['color'])) : ?>
            <span class="error"><?=$errors['color']?></span>
        <?php endif; ?>
    </p>

    <p>
    	<label for="cost">Cost</label>
        <input type="text" name="cost" id="cost" value="<?php
                if(!empty($_POST['cost'])) {
                    echo esc_attr($_POST['cost']);
                }
            ?>" />
        <?php if(!empty($errors['cost'])) : ?>
            <span class="error"><?=$errors['cost']?></span>
        <?php endif; ?>
    </p>

    <p>
    	<label for="price">Price</label>
        <input type="text" name="price" id="price" value="<?php
                if(!empty($_POST['price'])) {
                    echo esc_attr($_POST['price']);
                }
            ?>" />
        <?php if(!empty($errors['price'])) : ?>
            <span class="error"><?=$errors['price']?></span>
        <?php endif; ?>
    </p>

    <button>Submit</button>
  </form>
</body>
</html>