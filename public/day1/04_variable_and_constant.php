<?php 

include __DIR__ . "/../../config.php";
// AFter including the config file , we have access to our constants.

echo DB_DSN;
echo "<br />";
echo DB_USER;
echo "<br />";
echo DB_PASS;
echo "<br />";
/*
  varable and constants

  varable: 

  variable name in PHP

  all variables name strat with "$" sign.
  followed by a-zA-Z_
  folowed by a-zA-Z0-9_

  // legal varable name
     $first_name	
     $FirstName
     $firstname
     $_FIRST_NAME
     $_1planet
     $$name // varable and variable;
   // illegal variable name
      $first-name
      $1planet
      first_name

      To create constant , we use the 'define' function 
      constants should be used to avoid using MAGIC NUMBERS
      eg $g = $price * .05;

 */
define('GST',.05);

//NOTE: once defined, constants can be referenced WITHOUT
// a dollar sign 
$price= 10;
$tax1 = $price * GST;
echo $tax1;
//constants are good g=for confrigation option that will not change

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
</head>
</body>
</html>