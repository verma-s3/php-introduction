<?php

// We can output numbers, exoressions, strings.......... anything that can be displayed 
//in HTML. the following lines will be output BEFORE the DOCTYPE.... not generally a good idea.
echo 5;
echo "<br />";
echo  6+5;
echo "<br />";



?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
  <?php

    // echo is a language structure/feature.
    // and does not return a  value or require paraenthesis.
    echo "<h1>Generating Output</h1>";

    // print() always return a value of 1
    print("<p>This is with print tag</p>");
  ?>
</body>
</html>

<?php


show_source(__FILE__);