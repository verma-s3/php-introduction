<?php
  include __DIR__ . "/config.php";

  // constant decalaration
  define('tagline_text','PHP INRODUCTION');
  // variables declaration 
  $title = "In Class Assignment 2";
  $tagline = 'Web Page with PHP';
  $copyright  = "copyright &copy;, 2019 by Sonia Verma";
  $img = 'photo.png';
  $paragraph1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  $paragraph2 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title><?=$title?></title>
  <style>
    header,footer{
      width: 100%;
      height: 100px;
      line-height: 100px;
      text-align: center;
      background: #cfc;
    }

    header{
      text-align: left;
      padding-left: 130px;
      font-size: 42px;
      font-weight: bold;
    }

    img{
      height: 50%;
      display: block;
  	  margin-left: auto;
  	  margin-right: auto;
  	  width: 50%;
  	  margin-bottom: 20px;
    }

    span{
    	font-size: 18px;
    	color: #f00;
    }

  </style>
</head>
<body>
	<header><?=tagline_text?></header>
    <div>
      <h1><?=$tagline?></h1>
      <span><?="(".$title."---->".tagline_text.")"?></span>
      <p><?=$paragraph1?></p>
      <p><?=$paragraph2?></p>
      <img src='<?=$img?>' alt ='pic' />
    </div>
	<footer><?=$copyright?></footer>
</body>
</html>