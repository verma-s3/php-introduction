<?php

//Basic Debugging
//PHP offers a handful od=f simple function to assist in debugging 
//our code
//
// var_dump() // dump info about variable and its data types
// print_r() // dump info about variable with its structure (but not data types)
// var_export() // dump code reuired to re-create the variable 
// die() // kills script at the point
// exit() // same as die - same function, different namae

$num1 = 16;
$num2 = 22.9;
$name = 'happy Gilmore';
$fruits = array('apple','orange','banana');
$user = array(
        'first' => 'Happy',
        'last' => 'Gilmore',
        'age' => '12',
        'hobbies' => array('reading','cycling', 'movies')
        );
echo $num1 . '<br />';
echo $num2 . '<br />';
echo $name . '<br />';
// echo $fruits . '<br />';
// echo $user . '<br />';

var_dump($num1);
var_dump($num2);
var_dump($name);
var_dump($fruits);
var_dump($user);

?>