<?php
  //String in PHP

//config.php shouold not be visible to a web browser
  include __DIR__ . '/../../config.php';
  $title = 'Strings in PHP';

// Strings in PHP come in two varieties

  // Literal Strings

  // Strings enclosed in single quotes ''
  $string1 = 'the title of this page is $title';
  // interpolated Strings
  // Strings enclosed in double quotes ""
  $string2 = "the title of this page is $title";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title><?=$title?></title>
</head>
</body>
  <h1><?php echo $title?></h1>
  <p>Literal String: <?=$string1?></p>
  <p>Interpolated String: <?=$string2?></p>
</html>