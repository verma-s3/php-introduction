
	<div class="errors">
		<p>Please correct the following errors in your submission.</p>
		<ul>
			<?php foreach ($errors as $error): ?>
				<li><?=$error?></li>
			<?php endforeach; ?>
		</ul>
	</div>
