<?php

//PHP has two types of array
//
//
$nums = array();

$nums[] = 2; // index 0
$nums[] = 3; // index 1
$nums[] = 7;
$nums[] = 99;
$nums[19] = 33;// index is a9 but length is 5 here not like javascript

//Associative Array 

$city_pop = [];
$city_pop['winnipeg'] = 900000;
$city_pop['brandon'] = 456113;
$city_pop['selsirk'] = 4564133;

// The array assignment operator (when used inside an array constructor function) is => 

$user = array(
  'first' => 'Dave',
  'last' => 'Johnes'
);

// php's version of push, pop, shift, unshift
// // in_array, array_key_exists
// c
// count, size_of (size_of is an alias of count), array_map 
// //array Sortung funciton
// sort, asort, arsort, ksort, krsort
// // most array functions work directly on the array by refernce, meaning the value of the array will change when you 
// run the funciton......... but not all of them.
// 
// use a loopto create an array that contains the numbers 1 to 100 inclusive, then var_dump the resulting array.
// 
// Create an array repreents your favoutite moviw with attributes as keys. var_dump the array.
// 


// 1st query
$numbers_array = [];
for($i=1; $i<=100; $i++){
	$numbers_array[] = $i;
}
var_dump($numbers_array);

// 2nd query
$movie= array(
  'title' => 'Gold',
  'year' => '2018',
  'director'=>'Sajid Khan',
  'distributor' => 'T-Series',
  'star' => 'Akshay Kumar'
);

var_dump($movie);


// 3rd Query

$users = array();
$users[] = array('name' => 'sonia' , 'email' => 'sonia@gmail.com');
$users[] = array('name' => 'nisha' , 'email' => 'nisha@gmail.com');
$users[] = array('name' => 'jiya' , 'email' => 'jiya@gmail.com');

var_dump($users);
echo '<ul>';
foreach($users as $user){
	echo "<li>{$user['name']},{$user['email']}</li>";
}
echo '</ul>';
?>