<?php
// use an if statemnt to test if a query string varable named 'id' has been passed in URL , if so, output the id. if not, output an error 
// // message: please pass an id 
if(empty($_GET['id'])){
	echo 'Please pass an id';
}
else{
	echo "This id in the query is: {$_GET['id']}";
}

// if else statement
if('POST' == $_SERVER['REQUEST_METHOD']){
	echo "<p>post</p>";
}
elseif('PUT' == $_SERVER['REQUEST_METHOD']){
	echo "<p>put</p>";
}
elseif('DELETE' == $_SERVER['REQUEST_METHOD']){
	echo "<p>delete</p>";
}
else{
	echo "<p>get</p>";
}

//Switch Case
switch($_SERVER['REQUEST_METHOD']){
	case 'POST':
	  echo '<p>Post</p>';
	  break;

	case 'PUT' :
	  echo '<p>Put</p>';
	  break;
	  
	case 'DELETE':
	  echo '<p>Delete</p>';
	  break;
	  
	default:
	  echo '<p>Get</p>';
}

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
</head>
<body>
	<form action="03_conditinal.php" method="get">
        <label for="id">Search</label>
		<input type="text" name="id" />
		<button>Search</button>
	</form>
	
</body>
</html>