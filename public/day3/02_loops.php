<?php

//loops
//for loop
//while loop
//do while loop
//foreach loop
//write a for loop that outputs the numbers between 1 and 100 incluisve (each number on itw own line)
//
//write a while loop that outputs only odd number between o and 50 inclusive.
//
//write the same loop with do while loop
//
//weite a foreach loop that outputs the following array, including the keys, in a table with two columns
//
$users = array(
  'first_name' => 'Dave',
  'last_name' => 'Jones',
  'email' => 'deav@gmail.com',
  'city' => 'Winnipeg',
  'country' => 'Canada'
);





//?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Loops ih php</title>
  <style type="text/css">
  	table,tr,td{
  		border: 2px solid;
  		border-spacing: 5px;
  		padding: 2px;
  	}
  </style>
</head>
<body>
	<h2>For Loop</h2>
    <?php
      for($i=1; $i<=100; $i++){
		echo  "<br />". $i  ;
	  }
    ?>

	<h2>While Loop</h2>
	<?php
		$i=0;
		while($i<=50){
			if($i%2 != 0){
			  echo $i . "<br />";
		    }
			$i++;
		}
	?>

	<h2>Do While Loop</h2>
	<?php
        $i=0;
		do{
			if($i%2 != 0){
			  echo $i . "<br />";
		    }
			$i++;
		}while($i<=50);
	?>

	<h2>Foreach Loop</h2>
	<table>
		<?php foreach($users as $key => $value) : ?>
		<tr>	         
			<th><?=$key?></th>
			<td><?=$value?></td>
		</tr>
	<?php endforeach; ?>
	</table>
</body>
</html>