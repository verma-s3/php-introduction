<?php
require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';
//dd($_POST);
//
//
//set empty $errors array

$errors =[];

if('POST' == $_SERVER['REQUEST_METHOD']){
   $error = [];
   foreach($_POST as $key => $value){
    // if a field has an empty value
    // set an error for that field
    if(empty($value)){
      $errors[$key] = "$key is a required field";
    }//end if
    }


if(empty($errors)){
  $query = 'INSERT INTO 
            publisher
            (name, city, phone)
            Values
            (:name, :city, :phone)';

  $stmt = $dbh->prepare($query);
  

  $params = array(
      ':name' => $_POST['name'],
      ':city' => $_POST['city'],
      ':phone' => $_POST['phone']

  );

  $stmt->execute($params);
  $id = $dbh->lastInsertId();
  
  if($id){
     header('Location: show_reocrd.php?publisher_id=' . $id);
     exit;
  }
  else{
    die('There was a problem inserting the record.');
  }
}
}// end.post
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>ADD A Publisher</title>
</head>
<body>
	<h1>Add A Publisher</h1>

  <?php if(!empty($errors)) :?>
     <?php include 'errors.inc.php'; ?>
  <?php endif; ?>

	<form action="<?=$_SERVER['PHP_SELF']?>" method="Post">
    	<p>
    		<label for="name">Publisher Name </label>
    		<input type="text" name="name" id="name"/>
    	</p>

    	<p>
    		<label for="city">City </label>
    		<input type="text" name="city" id="city"/>
    	</p>

    	<p>
    		<label for="phone">Phone </label>
    		<input type="text" name="phone" id="phone"/>
    	</p>
    	<button>Submit Form</button>
	</form>
</body>
</html>