<?php

require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';

//1. create DB Connection
$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);


// 2. Write query to gat all books with their author, format, genre, and publisher, title, author.name, format.name, grnre.nams, publisher.name

$query = 'SELECT
             book.title,
             author.name as author,
             format.name as format,
             genre.name as genre, 
             publisher.name as publisher,
             book.price
             FROM 
             book
             JOIN author USING(author_id)
             JOIN format USING(format_id)
             JOIN genre USING(genre_id)
             JOIN publisher USING(publisher_id)
             ORDER BY title ASC';

// prepare the query
$stmt = $dbh->prepare($query);

// 4. Execute the query
$stmt->execute();


// 5. Fetch the results
//fetch-- getting a single results
//fetchAll -- get multiple
//// we cn specify the type of result, by passing in a parameter:
/////Numerical Array: PDO: FETCH_NUM
///Associative Array: PDO: FETCH_ASSOC
// Object : PDO::FETCH_OBJ
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);


dd($dbh);
dd($result);