<?php
  require __DIR__ . '/../../../config.php';
  require __DIR__ . '/../../../functions.php';
// empty errors array
$errors = [];
// test for POST
// 
// If post, ensure all fields are filled in 
// // else ser an error in the errors array.
//end test for POst
if('POST' == $_SERVER['REQUEST_METHOD']){
  //var_dump($_POST);

	$error = [];
	//Loop through the $_POST array
	foreach($_POST as $key => $value){
		// if a field has an empty value
		// set an error for that field
		if(empty($value)){
			$errors[$key] = "$key is a required field";
		}//end if
    }
}

//var_dump($errors);


?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
  <style type="text/css">
  	.errors{
  	   padding: 20px;
       background: #ccc;
       color: #900;
       border: 2px solid;
       font-weight: 500;
  	}
  	.error{
       color: #900;
  	}
  </style>
</head>
<body>
	<h1>Search Form</h1>
	<?php if(!empty($errors)) : ?>
		<div class="errors">
			<p>Please correct the following errors in your submission.</p>
			<ul>
				<?php foreach ($errors as $error): ?>
					<li><?=$error?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	<form method="post" action="assignment_day_3.php">
		<p>
		   <label for="name">Name</label>
		   <input type="text" name="name" id="name" value="<?php 
		     if(!empty($_POST['name'])){
                echo esc_attr($_POST['name']);
		     }
		    ?>" />
		    <?php if(!empty($errors['name'])) : ?>
		    	<span class="error"><?=$errors['name']?></span>
		    <?php endif; ?>
        </p>

        <p>
		   <label for="email">Email</label>
		   <input type="text" name="email" id="email" value="<?php 
		     if(!empty($_POST['email'])){
                echo esc_attr($_POST['email']);
		     }
		    ?>"/>
        </p>

        <p>
		   <label for="city">City</label>
		   <input type="text" name="city" id="city" value="<?php 
		     if(!empty($_POST['city'])){
                echo esc_attr($_POST['city']);
		     }
		    ?>"/>
        </p>

        <p>
		   <label for="phone">Phone</label>
		   <input type="number" name="phone" id="phone" value="<?php 
		     if(!empty($_POST['phone'])){
                echo esc_attr($_POST['phone']);
		     }
		    ?>"/>
        </p>

        <p>
		   <label for="password">Password</label>
		   <input type="password" name="password" id="password" value="<?php 
		     if(!empty($_POST['password'])){
                echo esc_attr($_POST['password']);
		     }
		    ?>"/>
        </p>
		
		<button>Submit</button>
	</form>
</body>
</html>