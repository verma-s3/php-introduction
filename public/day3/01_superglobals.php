<?php
//Superglobals

//superglobal array system arrays, with information accesible everywhere in an application. They primarly contain
// information from user(browser) input, and/or system/server inforamtion. they are all associative arrays with string kets. 
//IN PHP , associative arrays are actually arrays, not objects.


// All the Superglobals, with the exception of one , start with $_

//$_POST - contains data (key value pairs) from forms
//$_GET  - contains data (key calue pairs) from URL query string.
//$_SERVER - contains detailed server info 

echo '<pre>';
echo $_SERVER['REQUEST_METHOD'];

echo '<br />';

//print_r($_SERVER);
if($_SERVER['REQUEST_METHOD'] == 'GET'){
	echo '<p>This is get request</p>';
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	echo '<p>This is post request</p>';
}