<?php
require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';
// All on one page

// 1. Create HTML for form to create a  new format
//   -- Output errors using errors.inc.php
//   -- form should be sticky.
//   -- novalidate must be there.
//   
// 2. Process the form submission
// // -- name is required
// -- name must be atleast 3 characters long
// 
	// 3. if no errros
	//  -- create your prepared statement (query)
	//  -- bind your paramters
	//  -- insert the new record
	//  -- get the last inset id
	//  
	//  4. if the inert was successsful
	//  -- redirect to another page
	//  -- select hte new format from the databse
	//  -- display the new format on the page (var_dump)
	//  else;
	//  -- add an error to the errors array 'There was a problem adding the record'.

$errors = [];

if('POST' == $_SERVER['REQUEST_METHOD']){
  
  
  	if(empty($_POST['name'])){
    	$errors['name'] = "Please fill the name. It is a required field. ";
  	}
  	elseif(strlen($_POST['name'])< 3){
  		$errors['name'] = "name must be atleast 3 characters long";
  	}
  

  if(!count($errors)){
	  $query = 'INSERT INTO 
	            format
	            (name)
	            Values
	            (:name)';

	  $stmt = $dbh->prepare($query);
	  

	  $params = array(
	      ':name' => $_POST['name']
	  );

	  $stmt->execute($params);
	  $format_id = $dbh->lastInsertId();
	  
	  if($format_id){
	     header('Location: form_display.php?publisher_id='.$format_id);
	     exit;
	  }
	  else{
	    die('There was a problem inserting the record.');
	  }
	}// end of empty errors
}// end of POST post



?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>IN Class Assignment Day4 </title>
  <style type="text/css">
  	.errors{
  	   padding: 20px;
       background: #ccc;
       color: #900;
       border: 2px solid;
       font-weight: 500;
  	}
  	.error{
       color: #900;
  	}
  </style>
</head>
<body>
	<?php include 'errors.inc.php'; ?> 
	<form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
		<p>
			<lable for="name">Name</lable>
			<input type="Text" value="" name="name" id="name" />
			
		</p>
 		<button>Add name to Format</button>
	</form>
</body>
</html>