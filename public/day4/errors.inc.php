<?php if(!empty($errors)) :?>
	<div class="errors">
		<p>Please correct the following errors in your submission.</p>
		
			<?php foreach ($errors as $error): ?>
				<li><?=$error?></li>
			<?php endforeach; ?>
		
	</div>
<?php endif; ?>