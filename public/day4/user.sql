-- user table for books

USE booksite;
DROP TABLE IF EXISTS users;
CREATE TABLE users(
user_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(255),
last_name VARCHAR(255),
email VARCHAR(255),
age INT,
password VARCHAR(255),
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

