<?php 
require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';
// PHP Filter validators
// 
// Create a form with th efollowing data.
// 		-- first_name , last_name, email, age, password, password_confirm
// 		// all fields shoud be type text, except for password fields
// 		// which should be type passwod
// 		// include errors include above form 
// 		// make form sticky 
 
$error = [];
if('POST'== $_SERVER['REQUEST_METHOD']){
  var_dump($_POST);
  die('here');
}

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Registration form</title>
</head>
<body>
	<h1>Registration form</h1>
	<!-- <?php //include 'errors.inc.php'; ?> -->
	<form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
		
		<p>
			<label for="first_name">First Name</label>
			<input type="text" name="first_name" id="first_name" value="<?=clean('first_name');?>" />
		</p>

		<p>
			<label for="Last_name">Last Name</label>
			<input type="text" name="last_name" id="last_name" value="<?=clean('last_name');?>" />
		</p>

		<p>
			<label for="email">Email</label>
			<input type="text" name="email" id="email" value="<?=clean('email');?>" />
		</p>

		<p>
			<label for="age">Age</label>
			<input type="number" name="age" id="age" value="<?=clean('age');?>" />
		</p>

		<p>
			<label for="password">Password</label>
			<input type="password" name="password" id="password"  />
		</p>

		<p>
			<label for="password_confirm">Password confirm</label>
			<input type="password" name="password_confirm" id="password_confirm"  />
		</p>

		<button type="submit">Submit</button>
	</form>
</body>
</html>