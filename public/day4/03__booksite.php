<?php 

require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';

// query the data bas efor all books(book_id, title, author, year_pubished )... requirs join to author table 
// 
// // output th elist in HTML Below
// 
$query = 'SELECT book.book_id,
          book.title, 
          author.name as author,
          book.year_published
          FROM
          book
          JOIN author USING(author_id)';
$stmt = $dbh->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll();

//dd($result);


?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Book list</title>
</head>
<body>
	<table>
       <tr>
       	 <th>Book Id</th>
       	 <th>Title</th>
       	 <th>Author</th>
       	 <th>Year Published</th>
       </tr>
       <?php foreach($result as $key => $book) : ?>
       <tr>
       	<td><?=$book['book_id']?></td>
       	<td><a href="04_bookform.php?book_id=<?=esc_attr($book['book_id'])?>"><?=$book['title']?></a></td>
		<td><?=$book['author']?></td>
		<td><?=$book['year_published']?></td>
       </tr>
   <?php endforeach; ?>
	</table>
</body>
</html>