<?php 
 
 
 require __DIR__ . '/../../config.php';
 require __DIR__ . '/../../functions.php';
 $errors = [];


 if('GET' == $_SERVER['REQUEST_METHOD']){
   if(empty($_GET['book_id'])){
 	die('Go back and <a href="03__booksite.php">pick a book to edit</a>');
   }
   $query = "SELECT * from book where book_id = :book_id";

   $stmt = $dbh->prepare($query);
   $params = array(':book_id'=> (int) $_GET['book_id']);
   $stmt->execute($params);
   $_POST = $stmt->fetch(PDO::FETCH_ASSOC);
   
   if(empty($_POST)){
     $errors[] = 'Sorry, no book found with this id';
   }
 }

 
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Book Form </title>
</head>
<body>
	<h1>EDIT BOOK</h1>
	<?php include __DIR__ . '/errors.inc.php'; ?>
	<form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post">
		<input type="hidden" name="book_id" value="<?=clean('book_id')?>" />
		<p>
			<label for ="title">Title</label>
			<input type="text"  name="title" value="<?=clean('title')?>" />
		</p>

		<p>
			<label for ="year_published">Year Published</label>
			<input type="text"  name="year_published" value="<?=clean('year_published')?>" />
		</p>

		<p>
			<label for ="num_pages">Total Pages</label>
			<input type="text"  name="num_pages" value="<?=clean('num_pages')?>" />
		</p>


		<p>
			<label for ="in_print">IN PRINT</label>
			<input type="text"  name="in_print" value="<?=clean('in_print')?>" />
		</p>

		<p>
			<label for ="price">Price</label>
			<textarea name="description"><?=esc($_POST['description'])?></textarea>
		</p>

		<p>
			<label for="image">Image</label>
			<input type="text" name="image" value="<?=clean('image')?>">
		</p>
        <button>Update Book</button>
	</form>
</body>
</html>