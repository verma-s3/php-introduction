<?php 
require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';
// PHP Filter validators
// 
// Create a form with th efollowing data.
// 		-- first_name , last_name, email, age, password, password_confirm
// 		// all fields shoud be type text, except for password fields
// 		// which should be type passwod
// 		// include errors include above form 
// 		// make form sticky 
 
$errors = [];
if('POST'== $_SERVER['REQUEST_METHOD']){
  if(empty($_POST['email'])){
  	$errors['email'] = "email field is required";
  }
  	elseif(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
  	$errors	['email'] = "Please provide validate email address" ;
  }

  // first_name validation
  if(empty($_POST['first_name'])){
  	$errors['first_name'] = "first_name is required";
  }
  	elseif(strlen($_POST['first_name']) <3 ){
  	$errors	['first_name'] = "Please provide validate first_name atleast 3 characters" ;
  }

  //last_name validation
  if(empty($_POST['last_name'])){
  	$errors['last_name'] = "last_name is required";
  }
  	elseif(strlen($_POST['last_name']) < 3){
  	$errors	['last_name'] = "Please provide validate first_name atleast 3 characters" ;
  }
  
  //age validation
  if(empty($_POST['age'])){
  	$errors['age'] = "age is required";
  }
  elseif(!filter_var($_POST['age'],FILTER_VALIDATE_INT)){
	$errors	['age'] = "Please enter a number";
  }
  elseif($_POST['age'] < 18){
	$errors	['age'] = "Please provide validate Age atleast 18 years" ;
  }
  
  // password validation
  if(empty($_POST['password'])){
    $errors['password'] = "password is required";
  }
  elseif(strlen($_POST['password']) < 6){
	$errors	['password'] = "Please provide validate password  for atleast 6 characters" ;
  }

  // password confirmation
  if(empty($_POST['password_confirm'])){
    $errors['password_confirm'] = "password_confirm is required";
  }
  elseif(strlen($_POST['password_confirm']) < 6){
	$errors	['password_confirm'] = "Please provide validate password_confirm  for atleast 6 characters" ;
  }
  // matching of password and password_confirm
  if($_POST['password'] != $_POST['password_confirm']){
  	$errors['password'] = "password does not match. Try again!!";
  }


if(!count($errors)){
	  $query = 'INSERT INTO 
	            users
	            (first_name, last_name, email, age, password)
	            Values
	            (:first_name, :last_name, :email, :age, :password)';

	  $stmt = $dbh->prepare($query);
	  

	  $params = array(
	      ':first_name' => $_POST['first_name'],
	      ':last_name' => $_POST['last_name'],
	      ':email' => $_POST['email'],
	      ':age' => $_POST['age'],
	      ':password' => $_POST['password']
	  );

	  $stmt->execute($params);
	  $user_id = $dbh->lastInsertId();
	  
	  if($user_id){
	     header('Location: form_display.php?user_id='.$user_id);
	     exit;
	  }
	  else{
	    die('There was a problem inserting the record.');
	  }
	}// end of empty errors
}// end of POST post

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Registration form</title>
</head>
<body>
	<h1>Registration form</h1>
	<?php include __DIR__ . '/errors.inc.php'; ?>
	<form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
		
		<p>
			<label for="first_name">First Name</label>
			<input type="text" name="first_name" id="first_name" value="<?=clean('first_name');?>" />
		</p>

		<p>
			<label for="Last_name">Last Name</label>
			<input type="text" name="last_name" id="last_name" value="<?=clean('last_name');?>" />
		</p>

		<p>
			<label for="email">Email</label>
			<input type="text" name="email" id="email" value="<?=clean('email');?>" />
		</p>

		<p>
			<label for="age">Age</label>
			<input type="text" name="age" id="age" value="<?=clean('age');?>" />
		</p>

		<p>
			<label for="password">Password</label>
			<input type="password" name="password" id="password"  />
		</p>

		<p>
			<label for="password_confirm">Password confirm</label>
			<input type="password" name="password_confirm" id="password_confirm"  />
		</p>

		<button>Submit</button>
	</form>
</body>
</html>

<!-- 
// first_name and last_name required
 // first_name ab=nd Last_naem validate as string // filter validate
// minimum length 3 characters

//age required
// make sure it's an integer
// must be  atleast 18 
 -->