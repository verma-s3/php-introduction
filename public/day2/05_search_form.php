<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
</head>
<body>
	<h1>Search Form</h1>
	<form method="post" action="07_process_form.php">
		<label for="q">Search</label>
		<input type="text" name="q" id="q"/>
		<button>Search</button>
	</form>
</body>
</html>