<?php 

//
$title= "Array in PHP";

//
$fruits = array('apple','orange','banana','mango');

//
$pets= ['dog','cat','bird','goldfish'];

// creating empty arrays
$books = [];
$books = array();

// equivalent of using array_push.
$books[] = 'Dune';
$books[] = 'Clifford the big red dog';
$books[] = 'Star Wars';
$books[] = 'Dune, Messiah';
/*
echo '<pre>'; // mac only
print_r($fruits);
print_r($pets);
print_r($books);
*/


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title><?=$title?></title>
</head>
<body>
	<h1><?=$title?></h1>
	<h2>Fruits</h2>
	<ul>
	    <?php foreach($fruits AS $key => $value): ?>
	      <li><?="$key is $value"?></li>
	    <?php endforeach; ?>
    </ul>
    
    <!-- Looping through an array with a regular for loop -->
    <h2>Pets</h2>
    <?php for($i=0; $i<count($pets); $i++) :?>
       <li><?="$i is {$pets[$i]}" ?></li>
    <?php endfor; ?>

    <h2>Books</h2>
</body>
</html> 