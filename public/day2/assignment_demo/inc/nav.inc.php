<nav>
   <ul>
   	 <li <?=($title=='Home page') ? 'class="current"' : "" ;?>><a href="index.php">Home</a></li>
     <li <?=($title=='About page') ? 'class="current"' : "" ;?>><a href="about.php">About</a></li>
     <li <?=($title=='Service page') ? 'class="current"' : "" ;?>><a href="service.php">Service</a></li>
     <li <?=($title=='Contact page') ? 'class="current"' : "" ;?>><a href="contact.php">Contact</a></li>
     <li <?=($title=='FAQ page') ? 'class="current"' : "" ;?>><a href="faq.php">FAQ</a></li>
   </ul>
</nav>