<?php

require __DIR__ . '/../../config.php';
require __DIR__ . '/../../functions.php';
// SuperGlobal Arrays
// PHP Superglobal arays are accessible EVERYWHERE - they are ALWAYS in scope
// 
// Super global arrays contain the data is paased using specific 
// reuested methods, or thought HTTP inputs (like COOKIE)
// 
// For OUR Form, ehich uses the 'get' request method, all the data passed by yhe form is accessible in the $_GET 
// 
// The Super Globals we will use are.............
// 
// $_GET -------- Get form Submission
// $_POST ------- Post from submission
// $_COOKIE ----- Cookie information form the user's browser
// $_FILE ------- Files 
// $_
// $_
// $_
// 
//var_dump($_GET);
//var_dump($_POST);
//var_dump($_REQUEST);

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Process Form</title>
</head>
<body>
	<h1>Process Form</h1>
	<!-- <p>Ollluuuuuuuuuuuuuuu..........</p> -->
	<h2>You searched for: <?=htmlentities($_POST['q'])?></h2>
	<h2>You searched for: <?=htmlentities($_POST['q'], ENT_QUOTES)?></h2>

	<h2>You searched for: <?=htmlspecialchars($_POST['q'])?></h2>
	<h2>You searched for: <?=htmlspecialchars($_POST['q'], ENT_QUOTES)?></h2>

	<h2>You searched for: <?=esc($_POST['q'])?></h2>
	<h2>You searched for: <?=esc_attr($_POST['q'], ENT_QUOTES)?></h2>

	
</body>
</html>