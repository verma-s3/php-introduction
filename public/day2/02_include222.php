<?php

$title = "include files once";

// include part 2
// PHP allows to limit the number of times a file can be loaded.
// we use the include_once to restrict loading to a single time.
// 

include_once 'inc/myfile.inc.php';