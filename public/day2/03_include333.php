<?php

// what happens when file doesnot existed. and php cant find  the fle to load?
// PHP displays a non- faltal error or warning, and continues the run the script as if nothing had happened.
include 'inc/myfile2.ic.php';

// ANother way to loasd files is with the require keyword
// this should be used, when you script should NOT continue 
// running., if the file cannot be found. For example: if your script depends on a database connectin, in an
// external file, you should use require.... so that the script will die.
// 
require 'inc/myfile2.inc.php';

// like include, require comes with reuire_once option, with the same rules as include_once ... inother words, it's usefuul ONLY if you use the require_once syntax consistently.
// 
// 
// we should only get to this point if there are not fatal errors.

echo '<h1>You are Here!</h1>';