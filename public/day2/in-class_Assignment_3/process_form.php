<?php

require __DIR__ . '/../../../config.php';
require __DIR__ . '/../../../functions.php';
// if(!isset($_POST['name'])){
// 	echo 'Name is a required field!';
// }

//var_dump($_POST);

// set an empty array to hold OUR error.
$error = [];
//Loop through the $_POST array
foreach($_POST as $key => $value){
	// if a field has an empty value
	// set an error for that field
	if(empty($value)){
		$errors[$key] = "$key is a required field";
	}//end if
}

// test if the errors array has any errors in it
if(!empty($errors)){
	var_dump($errors);
	// if so, output a message
    // we have errors
	die("you have some error to fix!");
}




?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Process Form</title>
</head>
<body>
	<h1>Form Information</h1>
	
    <ul>
    	<?php foreach($_POST AS $key => $value): ?>
	      <li><?="$key : $value"?></li>
	    <?php endforeach; ?>
    </ul>
	
</body>
</html>