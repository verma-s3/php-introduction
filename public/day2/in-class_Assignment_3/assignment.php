<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
</head>
<body>
	<h1>Search Form</h1>
	<form method="post" action="process_form.php">
		<p>
		   <label for="name">Name</label>
		   <input type="text" name="name" id="name"/>
        </p>

        <p>
		   <label for="email">Email</label>
		   <input type="text" name="email" id="email"/>
        </p>

        <p>
		   <label for="city">City</label>
		   <input type="text" name="city" id="city"/>
        </p>

        <p>
		   <label for="phone">Phone</label>
		   <input type="number" name="phone" id="phone"/>
        </p>

        <p>
		   <label for="password">Password</label>
		   <input type="password" name="password" id="password"/>
        </p>
		
		<button>Submit</button>
	</form>
</body>
</html>